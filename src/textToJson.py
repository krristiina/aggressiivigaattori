import json



#This function is used to write input to a file (input to be given in JSON format)
def writeToFile(input):
    file = open('../resources/interviewQuestions.json', 'w')
    file.write(input)
    file.close()



#This function gathers the data from a file and return them in categorized arrays
def dataToCategorizedArrays():
    with open('../resources/interview.txt') as questions:
        all = [line.strip() for line in questions]

    aboutyouindex = all.index("ABOUT YOU")
    basicindex = all.index("BASIC")
    behavioralindex = all.index("BEHAVIORAL")
    brainteaserindex = all.index("BRAINTEASER")
    salaryindex = all.index("SALARY")

    aboutYou = all[aboutyouindex:basicindex]
    basic = all[basicindex:behavioralindex]
    behavioral = all[behavioralindex:brainteaserindex]
    brainteaser = all[brainteaserindex:salaryindex]
    salary = all[salaryindex:]

    categorizedAll = aboutYou, basic, behavioral, brainteaser, salary
    return categorizedAll




#This function converts categorized arrays into JSON data and writes it to a file
def arrayToJson(arrays):
    """
    :param :arrays: a collection of categorized arrays
    """
    data = []

    for array in arrays:
        for item in array:
            if array[0] not in array[array.index(item)]:
                text = array[array.index(item)]
                tag = array[0].lower()
                data.append({'text': text, 'tag': tag})

    json_data = json.dumps(data)
    writeToFile(json_data)





arrayToJson(dataToCategorizedArrays())