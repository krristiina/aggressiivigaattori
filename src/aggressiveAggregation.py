import json
import random

with open('../resources/interviewQuestions.json') as questionData:
    questions = json.load(questionData)


with open('../resources/motivationalQuotes.json') as responseData:
    responses = json.load(responseData)


def getQuestion():
    question = random.choice(questions)
    return question['text']


def getResponse():
    response = random.choice(responses['quotes'])
    return response['quote']


def play(question, response):
    print question + " " + response




play(getQuestion(), getResponse())